package com.example.demo.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.example.demo.controller.request.DeviceCreateRequest;
import com.example.demo.controller.request.DeviceUpdateRequest;
import com.example.demo.dto.DeviceStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeviceControllerTest
{
	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;
	
	private static final String STATUS_MATCHER = "$.status";
	private static final String DATA_MATCHER = "$.data";
	private static final String SUCCESS_VALUE = "SUCCESS";

	@Before
	public void onSetup()
	{
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	public static final String URI = "/device/";

	@Test
	public void test1Create() throws Exception
	{
		DeviceCreateRequest deviceCreateRequest = new DeviceCreateRequest();
		deviceCreateRequest.setName("rakesh");
		deviceCreateRequest.setSecretKey("sk1");
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(MockMvcRequestBuilders.post(URI+"create")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(deviceCreateRequest)))
		.andExpect(status().isCreated())
		.andReturn();
	}
	
	@Test
	public void test2GetAll() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.get(URI+"getAll")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath(STATUS_MATCHER).value(SUCCESS_VALUE))
		.andExpect(jsonPath(DATA_MATCHER).exists())
		.andExpect(jsonPath(DATA_MATCHER).isNotEmpty())
		.andExpect(jsonPath(DATA_MATCHER+".[0].name").value("rakesh"))
		.andExpect(jsonPath(DATA_MATCHER+".[0].secretKey").value("sk1"))
		.andExpect(jsonPath(DATA_MATCHER+".[0].status").value("NEW"))
		.andReturn();
	}
	
	@Test
	public void test3ListDevices() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.get(URI+"listDevices")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath(STATUS_MATCHER).value(SUCCESS_VALUE))
		.andExpect(jsonPath(DATA_MATCHER).exists())
		.andExpect(jsonPath(DATA_MATCHER).isNotEmpty())
		.andExpect(jsonPath(DATA_MATCHER+".[0].name").value("rakesh"))
		.andExpect(jsonPath(DATA_MATCHER+".[0].status").value("NEW"))
		.andReturn();
	}
	
	@Test
	public void test4ListDevicesByStatus() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.get(URI+"listDevices/NEW")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath(STATUS_MATCHER).value(SUCCESS_VALUE))
		.andExpect(jsonPath(DATA_MATCHER).exists())
		.andExpect(jsonPath(DATA_MATCHER).isNotEmpty())
		.andExpect(jsonPath(DATA_MATCHER+".[0].name").value("rakesh"))
		.andExpect(jsonPath(DATA_MATCHER+".[0].status").value("NEW"))
		.andReturn();
	}
	
	@Test
	public void test5UpdateDevice() throws Exception
	{
		DeviceUpdateRequest deviceUpdateRequest = new DeviceUpdateRequest();
		deviceUpdateRequest.setSecretKey("sk1");
		deviceUpdateRequest.setStatus(DeviceStatus.OK);
		
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.patch(URI+"update")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsBytes(deviceUpdateRequest)))
		.andDo(print())
		.andExpect(status().isNoContent())
		.andReturn();
	}
}
