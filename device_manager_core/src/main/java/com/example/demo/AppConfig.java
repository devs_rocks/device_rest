package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.dto.DeviceDto;

@Configuration
public class AppConfig 
{
	@Bean
	public List<DeviceDto> getDeviceDto()
	{
		return new ArrayList<DeviceDto>();
	}

}
