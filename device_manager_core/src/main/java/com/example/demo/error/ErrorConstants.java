package com.example.demo.error;

import java.util.HashMap;
import java.util.Map;

public class ErrorConstants 
{
	/**
	 * Invalid Name.
	 */
	public static final String DC_INV_NAM_001 = "DC_INV_NAM_001";
	
	/**
	 * Invalid Secret Key.
	 */
	public static final String DC_INV_SEC_001 = "DC_INV_SEC_001";
	
	/**
	 * Invalid Status.
	 */
	public static final String DC_INV_STA_001 = "DC_INV_STA_001";
	
	private static Map<String, String> errorMap = new HashMap<String, String>() {
		{
			put(DC_INV_NAM_001, "Invalid Name.");
			put(DC_INV_SEC_001, "Invalid Secret Key.");
			put(DC_INV_STA_001, "Invalid Status.");
		}
	};
	
	public static String getErrorMessage(String errorCode)
	{
		return errorMap.get(errorCode);
	}

}
