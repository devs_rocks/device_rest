package com.example.demo.dto;

import java.util.Date;

public class DeviceDto 
{
	private String name;
	private String secretKey;
	private DeviceStatus status;
	private Date createdDate;
	
	public DeviceDto() {
		this.status = DeviceStatus.NEW;
		this.createdDate = new Date();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public DeviceStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceStatus status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
