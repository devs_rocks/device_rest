package com.example.demo.dto;

public enum DeviceStatus 
{
	NEW,
	OK,
	UNHEALTHY,
	STALE
}
