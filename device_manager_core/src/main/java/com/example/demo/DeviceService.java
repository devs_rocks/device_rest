package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.controller.response.DeviceListResponse;
import com.example.demo.dto.DeviceDto;
import com.example.demo.dto.DeviceStatus;

@Service
public class DeviceService 
{
	@Autowired
	private List<DeviceDto> deviceDtoList;
	
	public void createDevice(String name, String secretKey)
	{
		DeviceDto deviceDto = new DeviceDto();
		deviceDto.setName(name);
		deviceDto.setSecretKey(secretKey);
		
		deviceDtoList.add(deviceDto);
	}
	
	public List<DeviceDto> getAllDevices()
	{
		return deviceDtoList;
	}

	public List<DeviceListResponse> getListDevices() 
	{
		return deviceDtoList.stream()
				.map(device -> { return new DeviceListResponse(device.getName(), device.getStatus());
		}).collect(Collectors.toList());
	}
	
	public void updateDevice(String secretKey, DeviceStatus status)
	{
		deviceDtoList.forEach(device -> {
			if (device.getSecretKey().equals(secretKey)) {
				device.setStatus(status);
			}
		});
	}
	
	public List<DeviceListResponse> getListDevicesByStatus(String status) 
	{
		return deviceDtoList.stream()
				.filter(device -> device.getStatus().toString().equals(status))
				.map(device -> { return new DeviceListResponse(device.getName(), device.getStatus());
		}).collect(Collectors.toList());
	}

}
