package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.request.DeviceCreateRequest;
import com.example.demo.controller.request.DeviceUpdateRequest;
import com.example.demo.controller.response.ResponseDtoWrapper;
import com.example.demo.error.ErrorConstants;
import com.example.demo.service.DeviceService;

@RestController
@RequestMapping(value = "/device")
public class DeviceController 
{
	@Autowired
	private DeviceService deviceService;
	
	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDtoWrapper> create(@RequestBody DeviceCreateRequest deviceCreateRequest,
			@Valid BindingResult bindindResult)
	{
		ResponseDtoWrapper response = new ResponseDtoWrapper();
		
		if (bindindResult.hasErrors()) {
			bindindResult.getFieldErrors().forEach(fieldError -> {
				if (fieldError.getField().equals("name")) response.setError(ErrorConstants.DC_INV_NAM_001);
				if (fieldError.getField().equals("secretKey")) response.setError(ErrorConstants.DC_INV_SEC_001);
			});
			return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.BAD_REQUEST);
		}
		
		deviceService.createDevice(deviceCreateRequest.getName(), deviceCreateRequest.getSecretKey());
		
		return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/getAll", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDtoWrapper> getAll()
	{
		ResponseDtoWrapper response = new ResponseDtoWrapper();
		
		response.setData(deviceService.getAllDevices());
		
		return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listDevices", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDtoWrapper> listDevices()
	{
		ResponseDtoWrapper response = new ResponseDtoWrapper();
		
		response.setData(deviceService.getListDevices());
		
		return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listDevices/{status}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDtoWrapper> listDevicesByStatus(@PathVariable("status") String status)
	{
		ResponseDtoWrapper response = new ResponseDtoWrapper();
		
		response.setData(deviceService.getListDevicesByStatus(status));
		
		return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.OK);
	}
	
	@PatchMapping(value = "/update")
	public ResponseEntity<ResponseDtoWrapper> updateDevice(@RequestBody DeviceUpdateRequest deviceUpdateRequest, 
			@Valid BindingResult bindindResult)
	{
		ResponseDtoWrapper response = new ResponseDtoWrapper();
		
		if (bindindResult.hasErrors()) {
			bindindResult.getFieldErrors().forEach(fieldError -> {
				if (fieldError.getField().equals("secretKey")) response.setError(ErrorConstants.DC_INV_SEC_001);
				if (fieldError.getField().equals("status")) response.setError(ErrorConstants.DC_INV_STA_001);
			});
			return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.BAD_REQUEST);
		}
		
		deviceService.updateDevice(deviceUpdateRequest.getSecretKey(), 
				deviceUpdateRequest.getStatus());
		
		return new ResponseEntity<ResponseDtoWrapper>(response, HttpStatus.NO_CONTENT);
	}
	
}
