package com.example.demo.controller.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.validation.annotation.Validated;

import com.example.demo.dto.DeviceStatus;

@Validated
public class DeviceUpdateRequest 
{
	@NotNull
	private String secretKey;
	
	@NotNull
	@Pattern(regexp = "^(?!OK|UNHEALTHY)")
	private DeviceStatus status;
	
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public DeviceStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceStatus status) {
		this.status = status;
	}
	
	
}
