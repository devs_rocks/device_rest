package com.example.demo.controller.response;

import com.example.demo.dto.DeviceStatus;

public class DeviceListResponse 
{
	private String name;
	private DeviceStatus status;
	
	public DeviceListResponse(String name, DeviceStatus status) {
		super();
		this.name = name;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DeviceStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceStatus status) {
		this.status = status;
	}

}
