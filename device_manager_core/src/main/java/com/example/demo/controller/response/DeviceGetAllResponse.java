package com.example.demo.controller.response;

import com.example.demo.dto.DeviceStatus;

public class DeviceGetAllResponse 
{
	private String name;
	private String secretKey;
	private DeviceStatus status;
	
	public DeviceGetAllResponse(String name, String secretKey, DeviceStatus status) {
		super();
		this.name = name;
		this.secretKey = secretKey;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public DeviceStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceStatus status) {
		this.status = status;
	}
}
