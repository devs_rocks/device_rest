package com.example.demo.controller.response;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.error.ErrorConstants;
import com.example.demo.error.ErrorDto;

public class ResponseDtoWrapper 
{
	private List data = new ArrayList();
	private String status;
	private List<ErrorDto> errors = new ArrayList<ErrorDto>();
	
	public List getData()
	{
		return this.data;
	}
	public void setData(Object obj)
	{
		this.status = "SUCCESS";
		this.data.add(obj);
	}
	public void setData(List data) {
		this.status = "SUCCESS";
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public List<ErrorDto> getError()
	{
		return this.errors;
	}
	public ResponseDtoWrapper setError(String errorCode) {
		this.status = "ERROR";
		ErrorDto errorDto = new ErrorDto();
		errorDto.setCode(errorCode);
		errorDto.setMessage(ErrorConstants.getErrorMessage(errorCode));
		this.errors.add(errorDto);
		return this;
	}
}
