package com.example.demo.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.demo.dto.DeviceDto;
import com.example.demo.dto.DeviceStatus;

@Component
public class DeviceStatusScheduler 
{
	@Autowired
	private List<DeviceDto> deviceDtoList;
	
	@Scheduled(cron = "0/10 * * ? * *")
	public void checkDeviceStatus()
	{
		Date startDate = new Date();
		deviceDtoList.stream()
			.filter(isOkStatus().and(isUpdatedBeforeFiveMinutes(startDate)))
			.forEach(device -> device.setStatus(DeviceStatus.STALE));
	}

	public static Predicate<DeviceDto> isOkStatus()
	{
		return n -> n.getStatus().equals(DeviceStatus.OK);
	}
	
	public static Predicate<DeviceDto> isUpdatedBeforeFiveMinutes(Date startDate)
	{
		return n -> {
			Calendar fiveMinsAgo = Calendar.getInstance();
			fiveMinsAgo.setTime(startDate);
			fiveMinsAgo.add(Calendar.MINUTE, -5);
			return n.getCreatedDate().before(fiveMinsAgo.getTime());
		};
	}
	
}
